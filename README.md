<meta name="viewport" content="width=device-width, initial-scale=1.0">

<center><img src="imgs/logo.png"></center>

<h1 style="text-align:center;border:none;margin-bottom:25px;"><strong><span style="color:#d2282e">Akinon</span></strong> Setup Docs</h1>

# **Repo Index**

1. [HowTo write documentation](#how-to-write-doc)
2. [Git tagging and versioning of project](git-tagging)

# **Introduction**

This repo is for docs of Setup Team workflows. Every docs of subjects will be written in Markdown (named as `README.md`) and exported as HTML (named as `index.html`) to its own folder. This main doc explains relations, short descriptions of subjects and main HowTos.

# **<span style="color:#d2282e">HowTo</span> Use**

# **<span style="color:#d2282e">HowTo</span> Write Doc**
