<meta name="viewport" content="width=device-width, initial-scale=1.0">

<center><img src="https://www.akinon.com/static/img/logo.png"></center>

<h1 style="text-align:center;border:none;margin-bottom:25px;"><strong>Git Tagging and Project Versioning</strong></h1>

# **Index**

1. [Case Story](#example-case-informations)
2. [Understanding Tags and Versions](#understanding-tags-and-versions)
3. [HowTo - Step by Step](#span-style%22colord2282e%22howtospan-steps)
4. [How It Works](#how-is-omnibuild-works)

# **Example Case Informations**

- Project name: `akinon`
- Issue: `Header Development`
- Jira issue name: `S1-3333`
- Working directory: `/Users/akinon/`
- Last taken version tag: `akinon33`

# **Understanding Tags and Versions**

At <span style="color:#d2282e">AKINON</span>, we are working with [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). Every issues has its own branch named with the same name. In this case it's `S1-3333`.

After your development is done, you must do the followings with order. <span style="color:#d2282e">HowTo</span>s of every step will be detailed in this doc.

1. Commit and push your codes. ([go](#1-commiting-and-pushing))
2. Open Pull Request (PR) for your branch. ([go](#2-opening-pr))
3. Move your issue in Jira Board to `In Review` column. ([go](#3-moving-issue-to-in-review-column))
4. Beg for PR approves from 2 seperated developer teammates. ([go](#4-begging-for-approves))
5. The teammate who gave the second approval to your PR will move your issue in Jira Board to `Test Now` column. ([go](#5-moving-issue-to-test-now-column))
6. Call any tester teammate to test your development on your `localhost` on desktop and mobile devices. ([go](#6-local-test))
7. The tester who has `Tested and Approved` sticker on your forehead will move your issue in Jira Board to `Ready to Release` column. ([go](#7-the-holy-words-ready-to-release))
8. Building your development branch and giving it a tag. ([go](#8-building-and-tagging))
9. Announcing the tag in your team's tag channel in Slack (eg. `#team-setup-tag-cont`). ([go](#9-announce-the-tag))
10. After Jenkins done its job **successfully**, Release Manager will move your issue in Jira Board to `Released` column. ([go](#10-deployment-done))
11. Now, PM and testers will test the product. If they say there is no bug, believe them with your heart. Utopia is in your heart. ([go](#11-after-party))
12. Don't move your issue in Jira Board to `Done` column. PM will. ([go](#12-merge))
13. Your job is done. ([go](#13-done))

# **<span style="color:#d2282e">HowTo</span> Steps**

## **1. Commiting and Pushing**

Commit your works with a clear description. For the possibility of further layouting or fixing, add versions. Here is example commit messages for development steps.

> S1-3333: Main wrapper is done, Mega menu development [0]
>
> S1-3333: Submenu dynamisation is done, Mega menu development [1]
>
> S1-3333: Submenu border style fix, Header navigation mega menu development [FIX 0]

> **ATTENTION:** _Setup team is commiting and pushing only `static_omnishop`, `shomnipro` and `perfume_templates` folders except `static_omnishop/dist` folder. Other non-gitignored files will not be commited. `static_omnishop/dist` folder and `omnitron_project` submodule will be automatically commited later while versioning (see [#8](#8-building-and-tagging))._

## **2. Opening PR**

After your last commit is pushed, you will get a link in Terminal for `Pull Request`. If you miss it, you can open your PR from Bitbucket web app. Here is an example output:

<center><img src="https://i.ibb.co/QX09hMr/PR-example.png"></center>

Remember to add at least 2 teammates as reviewers.

## **3. Moving Issue to `In Review` Column**

Just move it. Don't forget to log time.

## **4. Begging for Approves**

You must have at least 2 approves to deploy your work. So, if you need approves, just beg from your teammates.

> **ATTENTION:** _Don't approve your teammates' PR before reviewing. Of course, you need to trust them but, they are trusting you, too. All humans can do mistakes. Find the mistakes and be ruthless about them. You can log your Review time in Jira Board, `Review (S1-3404)` card._

## **5. Moving Issue to `Test Now` Column**

You don't need to watch your PR. The second approver should move your issue to `Test Now` column in Jira Board. After that, the Tester will find you. You can't run. Don't even try. They may come to you with an army.

While waiting them, you can start another issue in another `SHOP_PORT`, differ from your local test port (usually it's `8083`, [click](https://bitbucket.org/akinonteam/dockerized_akinon/src/master/#markdown-header-parametreler_1) for <span style="color:#d2282e">HowTo</span>). If the project is the same with tested one, you can copy the project folder and rename it like `akinon-clone`. Using terminal will be more accurate for copying than Finder.

## **6. Local Test**

> _"Look to my coming on the first light of the fifth day, at dawn look to the east."_
>
> (Tester the White)

The Tester will find you and ask you if you are available for local tests.

Depends on your working location, you may need to connect Akinon VPN before local testing. Because, we are using Local Area Network (LAN) for connecting to each other's localhost. So, tester and you need to be on the same LAN. For all Akinon offices, `Akinon AP` network is on for all and serves a single, united LAN.

After making sure you're on the same LAN, you need to execute following command in UNIX/Linux terminal and find your LAN IP:

```bash
ifconfig
```

The output of this command will list available Network Interfaces of your machine.

```js
if(isAkinonVPNConnected) {
```

You will see that the last interface (usually named `utun2`) has an `inet IP` like `192.169.19.xx`. This is unique to your personal VPN configuration and is static. Unless something extraordinary happens, it will be never changed. So, your LAN IP is `192.169.19.xx`.

```js
} else {
```

You will see an interface (usually named `en0`) has an `inet IP` like `192.168.1.xx`. This is unique to your personal machine but it is not static. Usually, it will be changed after reboot, re-connection etc. So, you may need to be sure and execute `ifconfig` and find your LAN IP for every local tests.

```js
}
```

If you didn't change your test PORT (see [#5](#5-moving-issue-to-test-now-column)), you can send your test environment address to the Tester like below:

> PLS http://192.168.1.16:8083 for S1-3333

If you canged the port, replace `8083` with it.

## **7. The Holy Words, Ready to Release!**

The Tester will connect to your local environment and see whatever you can see in your localhost. So, if there are testable things, the Tester will test them and inform you about.

If there is something to be done, but, can't be seen in your localhost, then, you must inform the Tester about and ask if it can be tested on live.

After the Tester's approvement, the Tester will move your task to `Ready to Release` column in Jira Board.

The hardest part is done. You can celebrate!

From now on, forget your job. Release manager will do the rest.

## **8. Building and Tagging**

1. If it's time, Release Manager (RM) will create development version branch and deploy it. Before deployment, RM will build your work with `omni_build` for Dockerized Projects or `omnife_build` for Zero Projects.

    Clone [omni_build](https://bitbucket.org/akinonteam/omni_build/src/master/) repo to working directory with following command in terminal:

    ```bash
    git clone git@bitbucket.org:akinonteam/omni_build.git
    ```

    or if `omnife_build` repo is opened:

    ```bash
    git clone git@bitbucket.org:akinonteam/omnife_build.git
    ```

2. RM will stop NPM for your work and checkout all unnecessary staged changes with following command:

    ```bash
    git checkout -- .
    ```

    > ATTENTION: This command will delete all uncommited changes. Be careful!

3. RM will check the commit tree and find the last version tag of project from [https://bitbucket.org/akinonteam/akinon/commits/](https://bitbucket.org/akinonteam/omni_build/commits/). Generally, the last commit of merged version tag looks like below:

    <center><img src="https://i.ibb.co/YPYd7C2/Screen-Shot-2020-06-22-at-16-38-19.png"></center>

    If it looks like below (`bisse_dev_25` appears, this means development version branch is not merged), RM will know that if this tag won't merged. RM will merge this tag:

    <center><img src="https://i.ibb.co/Nr0Bb7p/Screen-Shot-2020-06-22-at-16-42-02.png"></center>

4. RM saw that the last tag is `akinon33`. Merged or unmerged, it doesn't matter. You will take `akinon34`. So, RM need to create `akinon_dev_34` branch as development version branch.

5. For doing this, RM will change directory to `omni_build` or `omnife_build` folder and execute following command in terminal:

    ```bash
    python run.py -d /Users/akinon/dockerized_akinon/shops/akinon -b S1-3333 -dev akinon_dev_34 -pm master -sm master
    ```

    > Dear RMs:
    >
    > - Replace `/Users/akinon/dockerized_akinon/shops/akinon` with actual project root directory.
    > - Replace `S1-3333` with actual issue branch name. You can add multiple brances with space seperated (`-b S1-3333 S1-4444`).
    > - Replace `akinon_dev_34` with new actual development version branch name.
    > - `-pm` means `project master`. If you need to pull the `master` branch of project, declare it as `master`. Some projects, like `Gant PL`, use the same repo with the main project (`Gant` for this one) but a different master branch (`master__pl` for this one). If the situation is like this one, replace `master` with actual master branch name, like `master__pl`.
    > - `-sm` means `submodule`. Every project has `omnitron_project` submodule, but, some of them may use different branch of this. If the project uses `master` branch of `omnitron_project`, declare `-sm` as `master`. If not, declare it what it needs to be (e.g. `-sm CORE-3434`).
    > - For detailed information about `omni_build` tool, [(in progress)](https://bitbucket.org/akinonteam/omni_build/src/master/).

6. There will be lots of outputs but in the end, there should be `Created akinon_dev_34 staging branch successfully.` message. If it is, skip to 8th step.

7. If it's not, there may be conflicts or NPM/Webpack errors. Check, fix and commit them in `akinon_dev_34` branch if it is created and execute the following command again, with `-ued` parameter. If dev branch is not created, use your issue branch for commit and don't pass `-ued`.

    ```bash
    python run.py -d /Users/akinon/dockerized_akinon/shops/akinon -b S1-3333 -dev akinon_dev_34 -pm master -sm master -ued
    ```

    Here, `-ued` means `use existing dev`.

8. The tool commited your dev branch. You can push it to git with:

    ```bash
    git push origin akinon_dev_34
    ```

9. Now, you need to give a tag for this last commit:

    ```bash
    git tag akinon34
    git push origin akinon34
    ```

    > NOTE: Be careful, the number of dev branch and tag is the same.

## **9. Announce the Tag**

Yay! You have the tag now! Now, you should announce it for deployment. Spam the channel, dear Release Master, you have the right!

## **10. Deployment Done**

Ask a friend to join [the Slack Channel of Rick Sanchez](https://akinon.slack.com/archives/G2SDJ7M54). There are Jenkins and Rick Sanchez bots to help you. Jenkins will tell you if deployment starts and ends (success and failure). If it says like `SUCCESSFUL: Job ... [akinon akinon34@omnishop xxxxx] ...`, your work has been deployed!

Check the work on live site and if it works as you expected, move the issue to `Released` column in Jira Board.

## **11. After Party**

After all these, tester and PM will test your job. If they approve the work, they will move the issue to `Done` column in Jira Board. If you are not living in utopia, there may be some bugs and the issue will be moved to `To Do` column in Jira Board by the Tester or PM.

## **12. Merge**

You don't need to wait for the issue is moved to `Done`. If the Tester confirms the merge process, merge the work with the following commands in terminal:

```bash
git checkout master
git merge akinon_dev_34
git push
```

> WARNING: There must not be `git pull` before `git push` warning! Otherwise, you are doing something wrong.

## **13. DONE**

Congratulations!

# **How is `omni_build` Works?**

`omni_build` is a script that automates creating a staging branch for deployment. It merges needed branches, updates submodules, builds NPM modules and commits them automatically.

For step by step explanation, here is an example command to execute in terminal:

```bash
python run.py -d /Users/akinon/dockerized_akinon/shops/akinon -b S1-3333 S1-3434 S1-9999 -dev akinon_dev_34 -pm master -sm master
```

Step by step explanation is below:

1. Project master (`master` of `akinon`) is `checkout`ed.
2. Project master is `pull`ed.
3. Submodule master (`master` of `omnitron_project`) is `checkout`ed.
4. Submodule master is `pull`ed.
5. If `-ued` argument is passed, existed dev branch is `checkout`ed. But, it's not passed.
6. So, new dev branch is created (`akinon_dev_34` of `akinon`) and `checkout`ed.
7. In a loop, all branches to be merged (`S1-3333`, `S1-3434` and `S1-9999` of `akinon`) is `fetch`ed in and `merge`d to dev branch, seperately.
8. `npm install`ed and `npm run build`ed in submodule static files directory (`omnitron_porject/static_core`).
9. `npm install`ed and `npm run build`ed in project static files directory (`static_omnishop`).
10. All changes are added and committed with `git commit -am "npm run build"`.
11. Applied hard reset and checked current status.
12. If there are some changes, builded files added to be committed with `git add -A` command in project directory.
13. Added files commited as "Added dist files."
14. Else, it will print: `Created akinon_dev_34 staging branch successfully.`

# **How is `omnife_build` Works?**

`omni_build` is a script that automates creating a staging branch for deployment. It merges needed branches, updates submodules, builds NPM modules and commits them automatically.

For step by step explanation, here is an example command to execute in terminal:

```bash
python run.py -d /Users/akinon/dockerized_akinon/shops/akinon -b S1-3333 S1-3434 S1-9999 -dev akinon_dev_34 -pm master -sm master
```

Step by step explanation is below:

1. Project master (`master` of `akinon`) is `checkout`ed.
2. Project master is `pull`ed.
3. Submodule master (`master` of `omnife_project`) is `checkout`ed.
4. Submodule master is `pull`ed.
5. If `-ued` argument is passed, existed dev branch is `checkout`ed. But, it's not passed.
6. So, new dev branch is created (`akinon_dev_34` of `akinon`) and `checkout`ed.
7. In a loop, all branches to be merged (`S1-3333`, `S1-3434` and `S1-9999` of `akinon`) is `fetch`ed in and `merge`d to dev branch, seperately.
8. `yarn install`ed and `yarn build`ed in project template directory (`template`).
9. All changes are added and committed with `git commit -am "yarn build"`.
10. Applied hard reset and checked current status.
11. If there are some changes, builded files added to be committed with `git add -A` command in project directory.
12. Added files commited as "Added dist files."
13. Else, it will print: `Created akinon_dev_34 staging branch successfully.`
