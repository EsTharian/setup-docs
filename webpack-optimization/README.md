<meta name="viewport" content="width=device-width, initial-scale=1.0">

<h1 style="text-align:center;border:none;margin-bottom:25px;"><strong>Git Tagging and Project Versioning</strong></h1>

# **Index**

[In Progress...]

# **Webpack Optimization and Bundle Minimization**

Until the real documentation is written, the mail from Muhammed Taha Ayan to all Setup Team may be a guide for this. This steps applied to Sportive project.

Documentation is in progress...

## **Mail**

**19 March 2020**

Merhabalar,

Sportive projemizde fork sonrası bize gelen bundle boyutu GZIP'siz haliyle 6.11 MB, GZIP'li haliyle 1.54 MB idi ve bundle etme süresi 120 saniyeydi. Dosyalar watch modu ile +30 saniyede bundle ediliyordu.

Dün yaptığım bundle küçültme ve Webpack optimizasyonu işlemleri için izlediğim adımlar aşağıdaki gibidir:

1. package.json dosyasından bxSlider, FontAwesome 4, kullanılmayan lodash modülleri gibi gereksiz modülleri kaldırdım.
2. Bunlardan kullanacaklarımızın alternatiflerini veya güncel versiyonlarını dosyaya yazdım.
3. Gerekli tüm modülleri en güncel versiyonuna çektim.
4. Geliştirme sürecimizde herkesin local ortamını bir tutmak için Development modüllerinin versiyonlarını o anki en güncel versiyona sabitledim.
5. Javascript ve less dosyalarından, kaldırılan gereksiz NPM modüllerinin import'larını kaldırdım veya yorum bloğuna aldım.
6. Bu haliyle hem production hem development modlarda Webpack ile bundle etmeye çalışarak modül güncellemelerinden kaynaklanan depreciated methodları düzelttim ve bunların performans güncellemelerini yaptım.
7. eslint ve babel'de gerekli düzenlemeleri yaptım. Preset'i güncelledim.
8. Önceden dev ve prod modlar için ayrı ayrı düzenlenip common ile ortak çalıştırılan Webpack config dosyalarını tek dosyaya indirdim.
9. NPM scriptlerini bu yeni config dosyasına göre düzenledim.
10. Webpack'te, minimized halleri doğrudan kullanılabilir olan jQuery, Moment, Swiper gibi büyük modülleri config.resolve.alias ile yönlendirerek bundle'ı epey küçülttüm.
11. Moment locale Webpack plugin'i ile Moment'ın sadece TR için gereken modüllerini yükleyerek bundle'ı yarı yarıya küçültüp 483 KB yaptım.
12. Production modu için TerserPlugin ve OptimizeCSSAssetsPlugin kullanarak hem JS hem de CSS dosyalarımızdan commentleri kaldırdım.
13. Source map'i sadece development mod'da, ayrı bir dosyada oluşturmasını sağladım.
14. PostCSS ile autoprefixer ekledim.

Böylece son haliyle:

- Development mode'da bundle etmek 15-22 saniye, bir dosyada değişiklik yapıldıktan sonra watch ile bundle yenilemek 2-5 saniye sürüyor
- Production mode'da bundle etmek 18-23 saniye sürüyor.
- Development mode'da bundle boyutu 464 KB iken production mode'da bundle boyutu 388 KB olmuştur.

Bilgilerinize sunarım,

İyi çalışmalar 😊
